﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class StereoscopicSkyboxEditor
{
    [MenuItem("Assets/Create/New Stereoscopic Cubemap")]
    public static void CreateStereoMap()
    {
        StereoscopicCubemap asset = ScriptableObject.CreateInstance<StereoscopicCubemap>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/NewStereoCubemap.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
