﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class StereoscopicCubemap : ScriptableObject
{
    public Cubemap leftEyeCubemap;
    public Cubemap rightEyeCubemap;
}
