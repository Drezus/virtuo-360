﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VRStandardAssets.Utils;

public class StereoSkyboxManager : MonoBehaviour
{
    public Shader skyboxShader;
    public VRCameraFade camFader;

    public Renderer leftSphere;
    public Renderer rightSphere;

    StereoscopicCubemap[] stereoCubemapsList;

    int currentScene = 0;

    bool fading = false;

	void Start ()
    {
        stereoCubemapsList = Resources.LoadAll<StereoscopicCubemap>("");

        ApplyCubemap(stereoCubemapsList[currentScene]);
    }
	
	void Update ()
    {
	    if(Input.GetMouseButtonDown(0) && !fading)
        {
            StartCoroutine(ChangeScene());
        }
	}

    void ApplyCubemap(StereoscopicCubemap stereoCM)
    {
        Texture leftCubemap = new Texture();
        Texture rightCubemap = new Texture();
        leftCubemap = stereoCM.leftEyeCubemap;
        rightCubemap = stereoCM.rightEyeCubemap;

        Material leftMat = new Material(skyboxShader);
        Material rightMat = new Material(skyboxShader);
        leftMat.SetTexture("_Tex", leftCubemap);
        rightMat.SetTexture("_Tex", rightCubemap);

        leftSphere.material = leftMat;
        rightSphere.material = rightMat;
    }

    IEnumerator ChangeScene()
    {
        fading = true;

        currentScene++;

        if(currentScene >= stereoCubemapsList.Length)
        {
            currentScene = 0;
        }

        yield return camFader.BeginFadeOut(false);

        ApplyCubemap(stereoCubemapsList[currentScene]);

        yield return camFader.BeginFadeIn(false);

        fading = false;

        StopCoroutine(ChangeScene());
    }
}
