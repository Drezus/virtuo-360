﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;

public class SplashManager : MonoBehaviour
{
    public VRCameraFade camFader;
    bool fading = false;

    public GameObject construLogo;

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !fading)
        {
            StartCoroutine(ChangeScene());
        }
    }

    IEnumerator ChangeScene()
    {
        fading = true;

        construLogo.gameObject.SetActive(true);

        yield return camFader.BeginFadeOut(false);

        SceneManager.LoadScene(1);

        StopCoroutine(ChangeScene());
    }
}
